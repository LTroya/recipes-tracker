package com.ltroya.recipestracker.recipelist;

import com.ltroya.recipestracker.entities.Recipe;

public class StoreRecipeInteractorImpl implements StoreRecipeInteractor{
    private RecipeListRepository repository;

    public StoreRecipeInteractorImpl(RecipeListRepository repository) {
        this.repository = repository;
    }

    @Override
    public void executeUpdate(Recipe recipe) {
        repository.updateRecipes(recipe);
    }

    @Override
    public  void executeDelete(Recipe recipe) {
        repository.removeRecipe(recipe);
    }
}
