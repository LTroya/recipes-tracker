package com.ltroya.recipestracker.recipelist;

import com.ltroya.recipestracker.entities.Recipe;

public interface StoreRecipeInteractor {
    void executeUpdate(Recipe recipe);
    void executeDelete(Recipe recipe);
}
