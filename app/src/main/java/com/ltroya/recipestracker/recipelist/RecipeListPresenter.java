package com.ltroya.recipestracker.recipelist;

import com.ltroya.recipestracker.entities.Recipe;
import com.ltroya.recipestracker.recipelist.events.RecipeListEvent;
import com.ltroya.recipestracker.recipelist.ui.RecipeListView;

public interface RecipeListPresenter {
    void onCreate();
    void onDestroy();

    void getRecipes();
    void removeRecipe(Recipe recipe);
    void toggleFavorite(Recipe recipe);
    void onEventMainThread(RecipeListEvent event);

    RecipeListView getView();
}
