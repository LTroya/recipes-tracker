package com.ltroya.recipestracker.recipelist;

public interface RecipeListInteractor {
    void execute();
}
