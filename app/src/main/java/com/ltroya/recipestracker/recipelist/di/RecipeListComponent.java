package com.ltroya.recipestracker.recipelist.di;

import com.ltroya.recipestracker.libs.di.LibsModule;
import com.ltroya.recipestracker.recipelist.RecipeListPresenter;
import com.ltroya.recipestracker.recipelist.ui.adapters.RecipesAdapter;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {RecipeListModule.class, LibsModule.class})
public interface RecipeListComponent {
    RecipesAdapter getAdapter();
    RecipeListPresenter getPresenter();
}
