package com.ltroya.recipestracker.recipelist.ui.adapters;

import com.ltroya.recipestracker.entities.Recipe;

public interface OnItemClickListener {
    void onFavClick(Recipe recipe);
    void onItemClick(Recipe recipe);
    void onDeleteClick(Recipe recipe);
}
