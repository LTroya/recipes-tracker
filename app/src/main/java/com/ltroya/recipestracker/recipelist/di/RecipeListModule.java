package com.ltroya.recipestracker.recipelist.di;

import com.ltroya.recipestracker.entities.Recipe;
import com.ltroya.recipestracker.libs.base.EventBus;
import com.ltroya.recipestracker.libs.base.ImageLoader;
import com.ltroya.recipestracker.recipelist.RecipeListInteractor;
import com.ltroya.recipestracker.recipelist.RecipeListInteractorImpl;
import com.ltroya.recipestracker.recipelist.RecipeListPresenter;
import com.ltroya.recipestracker.recipelist.RecipeListPresenterImpl;
import com.ltroya.recipestracker.recipelist.RecipeListRepository;
import com.ltroya.recipestracker.recipelist.RecipeListRepositoryImpl;
import com.ltroya.recipestracker.recipelist.StoreRecipeInteractor;
import com.ltroya.recipestracker.recipelist.StoreRecipeInteractorImpl;
import com.ltroya.recipestracker.recipelist.ui.RecipeListView;
import com.ltroya.recipestracker.recipelist.ui.adapters.OnItemClickListener;
import com.ltroya.recipestracker.recipelist.ui.adapters.RecipesAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RecipeListModule {
    RecipeListView view;
    OnItemClickListener clickListener;

    public RecipeListModule(RecipeListView view, OnItemClickListener clickListener) {
        this.view = view;
        this.clickListener = clickListener;
    }

    @Provides
    @Singleton
    RecipeListView providesRecipeListView() {
        return this.view;
    }

    @Provides
    @Singleton
    RecipeListPresenter providesRecipeListPresenter(EventBus eventBus, RecipeListView view, RecipeListInteractor listInteractor, StoreRecipeInteractor storeInteractor) {
        return new RecipeListPresenterImpl(eventBus, view, listInteractor, storeInteractor);
    }

    @Provides
    @Singleton
    RecipeListInteractor providesRecipeListInteractor(RecipeListRepository repository) {
        return new RecipeListInteractorImpl(repository);
    }

    @Provides
    @Singleton
    StoreRecipeInteractor providesStoreRecipeInteractor(RecipeListRepository repository) {
        return new StoreRecipeInteractorImpl(repository);
    }

    @Provides
    @Singleton
    RecipeListRepository providesRecipeListRepository(EventBus eventBus) {
        return new RecipeListRepositoryImpl(eventBus);
    }

    @Provides
    @Singleton
    RecipesAdapter providesRecipesAdapter(List<Recipe> recipeList, ImageLoader imageLoader, OnItemClickListener onItemClickListener) {
        return new RecipesAdapter(recipeList, imageLoader, onItemClickListener);
    }

    @Provides
    @Singleton
    OnItemClickListener providesOnItemClickListener() {
        return this.clickListener;
    }

    @Provides
    @Singleton
    List<Recipe> providesEmptyList() {
        return new ArrayList<Recipe>();
    }
}
