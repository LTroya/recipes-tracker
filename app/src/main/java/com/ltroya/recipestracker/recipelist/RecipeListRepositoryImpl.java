package com.ltroya.recipestracker.recipelist;

import com.ltroya.recipestracker.entities.Recipe;
import com.ltroya.recipestracker.libs.base.EventBus;
import com.ltroya.recipestracker.recipelist.events.RecipeListEvent;
import com.raizlabs.android.dbflow.list.FlowCursorList;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.Arrays;
import java.util.List;

public class RecipeListRepositoryImpl implements RecipeListRepository {
    private EventBus eventBus;

    public RecipeListRepositoryImpl(EventBus eventBus) {
        this.eventBus = eventBus;
    }

    @Override
    public void getSavedRecipes() {
        List<Recipe> recipes = SQLite.select()
                                .from(Recipe.class)
                                .queryList();
        post(RecipeListEvent.READ_EVENT, recipes);
    }

    @Override
    public void updateRecipes(Recipe recipe) {
        recipe.update();
        post();
    }

    @Override
    public void removeRecipe(Recipe recipe) {
        recipe.delete();
        post(RecipeListEvent.DELETE_EVENT, Arrays.asList(recipe));
    }

    private void post(int type, List<Recipe> recipeList) {
        RecipeListEvent event = new RecipeListEvent();
        event.setType(type);
        event.setRecipeList(recipeList);
        eventBus.post(event);
    }

    private void post() {
        post(RecipeListEvent.UPDATE_EVENT, null);
    }


}
