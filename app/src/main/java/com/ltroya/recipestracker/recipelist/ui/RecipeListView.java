package com.ltroya.recipestracker.recipelist.ui;

import com.ltroya.recipestracker.entities.Recipe;

import java.util.List;

public interface RecipeListView {
    void setRecipes(List<Recipe> data);
    void recipeUpdated();
    void recipeDeleted(Recipe recipe);
}
