package com.ltroya.recipestracker.recipelist;

import com.ltroya.recipestracker.entities.Recipe;

public interface RecipeListRepository {
    void getSavedRecipes();
    void updateRecipes(Recipe recipe);
    void removeRecipe(Recipe recipe);
}
