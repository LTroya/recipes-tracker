package com.ltroya.recipestracker.recipemain.ui;

public interface SwipeGestureListener {
    void onKeep();
    void onDismiss();
}
