package com.ltroya.recipestracker.recipemain;

import com.ltroya.recipestracker.entities.Recipe;

public class SaveRecipeInteractorImpl implements SaveRecipeInteractor{
    RecipeMainRepository repository;

    public SaveRecipeInteractorImpl(RecipeMainRepository repository) {
        this.repository = repository;
    }

    @Override
    public void execute(Recipe recipe) {
        repository.saveRecipe(recipe);
    }
}
