package com.ltroya.recipestracker.recipemain;

import com.ltroya.recipestracker.entities.Recipe;
import com.ltroya.recipestracker.recipemain.events.RecipeMainEvent;
import com.ltroya.recipestracker.recipemain.ui.RecipeMainView;

public interface RecipeMainPresenter {
    void onCreate();
    void onDestroy();

    void dissmissRecipe();
    void getNextRecipe();
    void saveRecipe(Recipe recipe);

    void onEventMainThread(RecipeMainEvent event);

    void imageReady();
    void imageError(String error);

    RecipeMainView getView();
}
