package com.ltroya.recipestracker.recipemain;

import com.ltroya.recipestracker.entities.Recipe;

public interface SaveRecipeInteractor {
    void execute (Recipe recipe);
}
