package com.ltroya.recipestracker.recipemain.di;

import com.ltroya.recipestracker.api.RecipeClient;
import com.ltroya.recipestracker.api.RecipeService;
import com.ltroya.recipestracker.libs.base.EventBus;
import com.ltroya.recipestracker.recipemain.GetNextRecipeInteractor;
import com.ltroya.recipestracker.recipemain.GetNextRecipeInteractorImpl;
import com.ltroya.recipestracker.recipemain.RecipeMainPresenter;
import com.ltroya.recipestracker.recipemain.RecipeMainPresenterImpl;
import com.ltroya.recipestracker.recipemain.RecipeMainRepository;
import com.ltroya.recipestracker.recipemain.RecipeMainRepositoryImpl;
import com.ltroya.recipestracker.recipemain.SaveRecipeInteractor;
import com.ltroya.recipestracker.recipemain.SaveRecipeInteractorImpl;
import com.ltroya.recipestracker.recipemain.ui.RecipeMainView;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RecipeMainModule {
    RecipeMainView view;

    public RecipeMainModule(RecipeMainView view) {
        this.view = view;
    }

    @Provides @Singleton
    RecipeMainView providesRecipeMainView() {
        return this.view;
    }

    @Provides @Singleton
    RecipeMainPresenter providesRecipeMainPresenter(EventBus eventBus, RecipeMainView view, SaveRecipeInteractor saveInteractor, GetNextRecipeInteractor getNextInteractor) {
        return new RecipeMainPresenterImpl(eventBus, view, saveInteractor, getNextInteractor);
    }

    @Provides @Singleton
    SaveRecipeInteractor providesSaveRecipeInteractor(RecipeMainRepository repository) {
        return new SaveRecipeInteractorImpl(repository);
    }

    @Provides @Singleton
    GetNextRecipeInteractor providesGetNextRecipeInteractor(RecipeMainRepository repository) {
        return new GetNextRecipeInteractorImpl(repository);
    }

    @Provides @Singleton
    RecipeMainRepository providesRecipeMainRepository(EventBus eventBus, RecipeService service) {
        return new RecipeMainRepositoryImpl(eventBus, service);
    }

    @Provides @Singleton
    RecipeService providesRecipeService() {
        return new RecipeClient().getRecipeService();
    }
}
