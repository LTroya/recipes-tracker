package com.ltroya.recipestracker.recipemain.di;

import com.ltroya.recipestracker.libs.base.ImageLoader;
import com.ltroya.recipestracker.libs.di.LibsModule;
import com.ltroya.recipestracker.recipemain.RecipeMainPresenter;
import com.ltroya.recipestracker.recipemain.ui.RecipeMainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {RecipeMainModule.class, LibsModule.class})
public interface RecipeMainComponent {
    //void inject(RecipeMainActivity activity);
    ImageLoader getImageLoader();
    RecipeMainPresenter getPresenter();
}
