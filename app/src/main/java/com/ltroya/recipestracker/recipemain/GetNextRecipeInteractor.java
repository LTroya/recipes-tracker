package com.ltroya.recipestracker.recipemain;

public interface GetNextRecipeInteractor {
    void execute();
}
