package com.ltroya.recipestracker;

import android.app.Application;
import android.content.Intent;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.ltroya.recipestracker.libs.di.LibsModule;
import com.ltroya.recipestracker.login.ui.LoginActivity;
import com.ltroya.recipestracker.recipelist.di.DaggerRecipeListComponent;
import com.ltroya.recipestracker.recipelist.di.RecipeListComponent;
import com.ltroya.recipestracker.recipelist.di.RecipeListModule;
import com.ltroya.recipestracker.recipelist.ui.RecipeListActivity;
import com.ltroya.recipestracker.recipelist.ui.RecipeListView;
import com.ltroya.recipestracker.recipelist.ui.adapters.OnItemClickListener;
import com.ltroya.recipestracker.recipemain.di.DaggerRecipeMainComponent;
import com.ltroya.recipestracker.recipemain.di.RecipeMainComponent;
import com.ltroya.recipestracker.recipemain.di.RecipeMainModule;
import com.ltroya.recipestracker.recipemain.ui.RecipeMainActivity;
import com.ltroya.recipestracker.recipemain.ui.RecipeMainView;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;


public class RecipesTrackerApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        initFacebook();
        initDB();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        DBTearDown();
    }

    private void DBTearDown() {
        FlowManager.destroy();
    }

    private void initDB() {
        FlowManager.init(new FlowConfig.Builder(this).build());
    }

    private void initFacebook () {
        FacebookSdk.sdkInitialize(this);
    }

    public void logout() {
        LoginManager.getInstance().logOut();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public RecipeMainComponent getRecipeMainComponent(RecipeMainActivity activity, RecipeMainView view) {
        return DaggerRecipeMainComponent
                .builder()
                .libsModule(new LibsModule(activity))
                .recipeMainModule(new RecipeMainModule(view))
                .build();
    }

    public RecipeListComponent getRecipeListComponent(RecipeListActivity activity, RecipeListView view, OnItemClickListener clickListener) {
        return DaggerRecipeListComponent
                .builder()
                .libsModule(new LibsModule(activity))
                .recipeListModule(new RecipeListModule(view, clickListener))
                .build();
    }
}
