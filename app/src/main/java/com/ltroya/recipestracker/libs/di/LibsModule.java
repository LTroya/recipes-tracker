package com.ltroya.recipestracker.libs.di;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.ltroya.recipestracker.libs.GlideImageLoader;
import com.ltroya.recipestracker.libs.GreenRobotEventBus;
import com.ltroya.recipestracker.libs.base.EventBus;
import com.ltroya.recipestracker.libs.base.ImageLoader;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class LibsModule {
    private Activity activity;

    public LibsModule(Activity activity) {
        this.activity = activity;
    }

    @Provides
    @Singleton
    ImageLoader providesImageLoader(RequestManager requestManager) {
        return new GlideImageLoader(requestManager);
    }

    @Provides
    @Singleton
    RequestManager providesRequestManager(Activity activity) {
        return Glide.with(activity);
    }


    @Provides
    @Singleton
    Activity providesActivity() {
        return this.activity;
    }


    @Provides
    @Singleton
    EventBus providesEventBus(org.greenrobot.eventbus.EventBus eventBus) {
        return new GreenRobotEventBus(eventBus);
    }

    @Provides
    @Singleton
    org.greenrobot.eventbus.EventBus providesLibraryEventBus() {
        return org.greenrobot.eventbus.EventBus.getDefault();
    }
}
