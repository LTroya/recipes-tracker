package com.ltroya.recipestracker.recipemain;

import com.ltroya.recipestracker.BaseTest;
import com.ltroya.recipestracker.entities.Recipe;

import org.junit.Test;
import org.mockito.Mock;

import static org.mockito.Mockito.verify;

public class SaveRecipeInteractorImplTest extends BaseTest{
    @Mock RecipeMainRepository repository;
    @Mock Recipe recipe;
    private SaveRecipeInteractor interactor;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        interactor = new SaveRecipeInteractorImpl(repository);
    }

    @Test
    public void testExecute_callRepository() throws Exception {
        interactor.execute(recipe);
        verify(repository).saveRecipe(recipe);
    }
}
