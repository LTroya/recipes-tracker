package com.ltroya.recipestracker.recipelist;

import com.ltroya.recipestracker.BaseTest;

import org.junit.Test;
import org.mockito.Mock;

import static org.mockito.Mockito.verify;

public class RecipeListInteractorImplTest extends BaseTest {
    @Mock private RecipeListRepository repository;

    private RecipeListInteractor interactor;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        interactor = new RecipeListInteractorImpl(repository);
    }

    @Test
    public void testExecute_ShouldCallRepository() throws Exception {
        interactor.execute();
        verify(repository).getSavedRecipes();
    }
}
