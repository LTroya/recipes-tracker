package com.ltroya.recipestracker.recipelist;

import com.ltroya.recipestracker.BaseTest;
import com.ltroya.recipestracker.entities.Recipe;

import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;

/**
 * Created by LTroya on 7/13/2016 AD.
 */
public class StoreRecipeInteractorImplTest extends BaseTest{
    @Mock private RecipeListRepository repository;
    @Mock Recipe recipe;
    private StoreRecipeInteractor interactor;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        interactor = new StoreRecipeInteractorImpl(repository);
    }

    @Test
    public void testExecuteUpdate() throws Exception {
        interactor.executeUpdate(recipe);
        verify(repository).updateRecipes(recipe);
    }

    @Test
    public void testExecuteDelete() throws Exception {
        interactor.executeDelete(recipe);
        verify(repository).removeRecipe(recipe);
    }
}